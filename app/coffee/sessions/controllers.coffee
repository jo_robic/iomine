'use strict'

angular.module('iomine.sessions').controller 'LoginCtrl', ($scope, $http, $q, $ionicLoading, settings, $state, $rootScope, $ionicPopup)->
  $scope.loginData = {}

  $scope.login = ()->
    deferred = $q.defer()

    $ionicLoading.show
      template: "Login..."

    login = angular.lowercase $scope.loginData.username

    $http (
      url: settings.apiUrl + '/users/current.json'
      method: 'GET'
      headers: {'Authorization': 'Basic ' + btoa("#{login}:#{$scope.loginData.password}")}
    )
    .success (res)->
      user = res.user
      user = _.extend user, {fullname: "#{user.firstname} #{user.lastname}"}


      console.log user
      localStorage.setItem('iomine.user', angular.toJson(user))


      deferred.resolve()

      $ionicLoading.hide()
      $state.go 'app.projects'

    .error ()->
      deferred.reject()
      $ionicLoading.hide()

      alertPopup = $ionicPopup.alert
        title: 'Authentification !'
        template: 'Identifiants erronées'

      alertPopup.then (res)->
        $scope.loginData = {}

    deferred.promise

angular.module('iomine.sessions').controller 'LogoutCtrl', ($scope, $state, $rootScope)->

  $scope.logout = ()->
    delete localStorage.removeItem('iomine.user')
    $state.go 'login'

  return $scope.logout()
