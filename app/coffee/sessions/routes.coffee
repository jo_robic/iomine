'use strict'

angular.module('iomine.sessions').config ($stateProvider, $urlRouterProvider) ->
  $stateProvider
    .state("login",
      url: "/app/login"
      templateUrl: "templates/login.html"
      controller: "LoginCtrl"
    )
    .state("logout",
      url: "/app/logout"
      controller: "LogoutCtrl"
    )

