'use strict'

angular.module('iomine.projects').config ($stateProvider, $urlRouterProvider) ->
  $stateProvider
    .state("app.projects",
      url: "/projects"
      views:
        menuContent:
          templateUrl: "templates/projects.html"
          controller: "ProjectsCtrl"
    )
    .state "app.project",
      url: "/projects/:projectId"
      views:
        menuContent:
          templateUrl: "templates/project.html"
          controller: "ProjectCtrl"
