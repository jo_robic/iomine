'use strict'

app = angular.module('iomine.projects')

app.controller('ProjectsCtrl', ($scope, Project, $http, settings, $location; $state)->
  $scope.projects = []

  console.log '--- STATE ---', $location, $state.current


  Project.getList().then (res)->
    $scope.projects = res


)

app.controller('ProjectCtrl', ($scope, Project, User, $stateParams)->

  Project.one($stateParams['projectId']).get().then (project)->
    $scope.project = project.project

    project.getList('issues').then (issues)->
      $scope.project.issues = issues

      _.each $scope.project.issues, (issues)->
        User.get(issues.assigned_to.id).then (res)->
          issues.assigned_to.email = res.user.mail
          issues.assigned_to.avatar = 'http://www.gravatar.com/avatar/' + CryptoJS.MD5(res.user.mail)

)
