'use strict'
# Ionic Starter App

# angular.module is a global place for creating, registering and retrieving Angular modules
# 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
# the 2nd parameter is an array of 'requires'
# 'starter.controllers' is found in controllers.js

# Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
# for form inputs)

# org.apache.cordova.statusbar required
angular.module("iomine", [
  "ionic",
  "restangular",
  "ngCookies",
  "iomine.systems",
  "iomine.projects",
  "iomine.users",
  "iomine.sessions"
])




.run(($ionicPlatform, $rootScope, $state, $location, Restangular, $ionicLoading, $document, $ionicPopup) ->

  currentUser = {}
  api_key = ''

  $ionicPlatform.ready ->
    cordova.plugins.Keyboard.hideKeyboardAccessoryBar true  if window.cordova and window.cordova.plugins.Keyboard
    StatusBar.styleDefault()  if window.StatusBar

    $ionicPlatform.registerBackButtonAction ()->
      if $state.current.name == 'app.projects' || $state.current.name == 'login'

        confirmPopup = $ionicPopup.confirm
           title: "Iomine App",
           template: "Êtes vous sûr de vouloir quitter l'application ?"

        confirmPopup.then (res)->
          navigator.app.exitApp() if res
      else
        navigator.app.backHistory()
    , 100

  getCurrentUser = =>
    store = localStorage.getItem('iomine.user')

    if store && _.isEmpty(currentUser)
      currentUser = angular.fromJson store
      api_key = currentUser.api_key

    else if !store
      currentUser = {}
      api_key = ''

    else
      return



  $rootScope.$on '$stateChangeStart', (event, toState, toParams, fromState, fromParams)->

    getCurrentUser()

    console.log 'change state', currentUser, !!api_key

    if !api_key && toState.url != '/app/login'
      console.log 'redirect login'
      event.preventDefault()
      $state.go 'login'

    else if toState.url == '/app/login' && !!api_key
      console.log 'go to projects'
      event.preventDefault()
      $state.go 'app.projects'


  Restangular.setFullRequestInterceptor (element, operation, route, url, headers, params, httpConfig)->
    $ionicLoading.show()
    # console.log '-------------------'
    # console.log 'restangular request'

    newHeaders = _.extend(headers, {'X-Redmine-API-Key': api_key })
    # newHeaders = _.extend(headers, {'X-Redmine-API-Key': '16c16056e259d4286740eb1fbba1de6531180e38' })
    # console.log JSON.stringify(newHeaders)


    return {
      element: element,
      params: params,
      headers: newHeaders
      httpConfig: httpConfig
    }

  Restangular.addResponseInterceptor (data, operation, what, url, response, deferred)->
    # console.log '-------------------'
    # console.log 'restangular response'
    # console.log JSON.stringify data
    $ionicLoading.hide()

     # console.log data, what
    if operation == "getList"
      newData = data[what]
      newData.offset = data.offset
      newData.limit = data.limit
      return newData
    else
      return data

)



.constant(
  'settings': {
    apiUrl: 'http://redmine.leikir-studio.com'
    # apiUrl: 'http://redmine.jonathanrobic.fr'
  }
)

.config( ($httpProvider)->
  $httpProvider.defaults.useXDomain = true
  delete $httpProvider.defaults.headers.common['X-Requested-With']
)

.config( (RestangularProvider, settings)->
  RestangularProvider.setBaseUrl settings.apiUrl
  RestangularProvider.setDefaultRequestParams('jsonp', {callback: 'JSON_CALLBACK'});
  RestangularProvider.setRequestSuffix('.json')
  RestangularProvider.setDefaultRequestParams('get', {limit: 100});
)

.config(($stateProvider, $urlRouterProvider) ->
  $stateProvider.state("app",
    url: "/app"
    abstract: true
    templateUrl: "templates/menu.html"
    controller: "AppCtrl"
  )

  # if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise "/app/projects"
)




angular.module('iomine.projects', [])
angular.module('iomine.users', [])
angular.module('iomine.systems', [])
angular.module('iomine.sessions', [])
