'use strict'

app = angular.module('iomine.systems')

app.controller 'AppCtrl', ($scope, $ionicModal, $timeout, $rootScope, $state, $cookieStore)->
  if localStorage.getItem('iomine.user')
    $rootScope.isAuthenticate = true
    $rootScope.currentUser = angular.fromJson(localStorage.getItem('iomine.user') )

  else
    $rootScope.isAuthenticate = false


