(function() {
  'use strict';
  var app;

  app = angular.module('iomine.systems');

  app.controller('AppCtrl', function($scope, $ionicModal, $timeout, $rootScope, $state, $cookieStore) {
    if (localStorage.getItem('iomine.user')) {
      $rootScope.isAuthenticate = true;
      return $rootScope.currentUser = angular.fromJson(localStorage.getItem('iomine.user'));
    } else {
      return $rootScope.isAuthenticate = false;
    }
  });

}).call(this);
