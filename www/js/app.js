(function() {
  'use strict';
  angular.module("iomine", ["ionic", "restangular", "ngCookies", "iomine.systems", "iomine.projects", "iomine.users", "iomine.sessions"]).run(function($ionicPlatform, $rootScope, $state, $location, Restangular, $ionicLoading, $document, $ionicPopup) {
    var api_key, currentUser, getCurrentUser;
    currentUser = {};
    api_key = '';
    $ionicPlatform.ready(function() {
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      }
      if (window.StatusBar) {
        StatusBar.styleDefault();
      }
      return $ionicPlatform.registerBackButtonAction(function() {
        var confirmPopup;
        if ($state.current.name === 'app.projects' || $state.current.name === 'login') {
          confirmPopup = $ionicPopup.confirm({
            title: "Iomine App",
            template: "Êtes vous sûr de vouloir quitter l'application ?"
          });
          return confirmPopup.then(function(res) {
            if (res) {
              return navigator.app.exitApp();
            }
          });
        } else {
          return navigator.app.backHistory();
        }
      }, 100);
    });
    getCurrentUser = (function(_this) {
      return function() {
        var store;
        store = localStorage.getItem('iomine.user');
        if (store && _.isEmpty(currentUser)) {
          currentUser = angular.fromJson(store);
          return api_key = currentUser.api_key;
        } else if (!store) {
          currentUser = {};
          return api_key = '';
        } else {

        }
      };
    })(this);
    $rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
      getCurrentUser();
      console.log('change state', currentUser, !!api_key);
      if (!api_key && toState.url !== '/app/login') {
        console.log('redirect login');
        event.preventDefault();
        return $state.go('login');
      } else if (toState.url === '/app/login' && !!api_key) {
        console.log('go to projects');
        event.preventDefault();
        return $state.go('app.projects');
      }
    });
    Restangular.setFullRequestInterceptor(function(element, operation, route, url, headers, params, httpConfig) {
      var newHeaders;
      $ionicLoading.show();
      newHeaders = _.extend(headers, {
        'X-Redmine-API-Key': api_key
      });
      return {
        element: element,
        params: params,
        headers: newHeaders,
        httpConfig: httpConfig
      };
    });
    return Restangular.addResponseInterceptor(function(data, operation, what, url, response, deferred) {
      var newData;
      $ionicLoading.hide();
      if (operation === "getList") {
        newData = data[what];
        newData.offset = data.offset;
        newData.limit = data.limit;
        return newData;
      } else {
        return data;
      }
    });
  }).constant({
    'settings': {
      apiUrl: 'http://redmine.leikir-studio.com'
    }
  }).config(function($httpProvider) {
    $httpProvider.defaults.useXDomain = true;
    return delete $httpProvider.defaults.headers.common['X-Requested-With'];
  }).config(function(RestangularProvider, settings) {
    RestangularProvider.setBaseUrl(settings.apiUrl);
    RestangularProvider.setDefaultRequestParams('jsonp', {
      callback: 'JSON_CALLBACK'
    });
    RestangularProvider.setRequestSuffix('.json');
    return RestangularProvider.setDefaultRequestParams('get', {
      limit: 100
    });
  }).config(function($stateProvider, $urlRouterProvider) {
    $stateProvider.state("app", {
      url: "/app",
      abstract: true,
      templateUrl: "templates/menu.html",
      controller: "AppCtrl"
    });
    return $urlRouterProvider.otherwise("/app/projects");
  });

  angular.module('iomine.projects', []);

  angular.module('iomine.users', []);

  angular.module('iomine.systems', []);

  angular.module('iomine.sessions', []);

}).call(this);
