(function() {
  'use strict';
  angular.module('iomine.users').factory('User', function(Restangular) {
    return Restangular.all('users');
  });

}).call(this);
