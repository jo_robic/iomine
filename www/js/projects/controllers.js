(function() {
  'use strict';
  var app;

  app = angular.module('iomine.projects');

  app.controller('ProjectsCtrl', function($scope, Project, $http, settings, $location, $state) {
    $scope.projects = [];
    console.log('--- STATE ---', $location, $state.current);
    return Project.getList().then(function(res) {
      return $scope.projects = res;
    });
  });

  app.controller('ProjectCtrl', function($scope, Project, User, $stateParams) {
    return Project.one($stateParams['projectId']).get().then(function(project) {
      $scope.project = project.project;
      return project.getList('issues').then(function(issues) {
        $scope.project.issues = issues;
        return _.each($scope.project.issues, function(issues) {
          return User.get(issues.assigned_to.id).then(function(res) {
            issues.assigned_to.email = res.user.mail;
            return issues.assigned_to.avatar = 'http://www.gravatar.com/avatar/' + CryptoJS.MD5(res.user.mail);
          });
        });
      });
    });
  });

}).call(this);
