(function() {
  'use strict';
  angular.module('iomine.projects').config(function($stateProvider, $urlRouterProvider) {
    return $stateProvider.state("app.projects", {
      url: "/projects",
      views: {
        menuContent: {
          templateUrl: "templates/projects.html",
          controller: "ProjectsCtrl"
        }
      }
    }).state("app.project", {
      url: "/projects/:projectId",
      views: {
        menuContent: {
          templateUrl: "templates/project.html",
          controller: "ProjectCtrl"
        }
      }
    });
  });

}).call(this);
