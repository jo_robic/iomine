(function() {
  'use strict';
  angular.module('iomine.sessions').controller('LoginCtrl', function($scope, $http, $q, $ionicLoading, settings, $state, $rootScope, $ionicPopup) {
    $scope.loginData = {};
    return $scope.login = function() {
      var deferred, login;
      deferred = $q.defer();
      $ionicLoading.show({
        template: "Login..."
      });
      login = angular.lowercase($scope.loginData.username);
      $http({
        url: settings.apiUrl + '/users/current.json',
        method: 'GET',
        headers: {
          'Authorization': 'Basic ' + btoa("" + login + ":" + $scope.loginData.password)
        }
      }).success(function(res) {
        var user;
        user = res.user;
        user = _.extend(user, {
          fullname: "" + user.firstname + " " + user.lastname
        });
        console.log(user);
        localStorage.setItem('iomine.user', angular.toJson(user));
        deferred.resolve();
        $ionicLoading.hide();
        return $state.go('app.projects');
      }).error(function() {
        var alertPopup;
        deferred.reject();
        $ionicLoading.hide();
        alertPopup = $ionicPopup.alert({
          title: 'Authentification !',
          template: 'Identifiants erronées'
        });
        return alertPopup.then(function(res) {
          return $scope.loginData = {};
        });
      });
      return deferred.promise;
    };
  });

  angular.module('iomine.sessions').controller('LogoutCtrl', function($scope, $state, $rootScope) {
    $scope.logout = function() {
      delete localStorage.removeItem('iomine.user');
      return $state.go('login');
    };
    return $scope.logout();
  });

}).call(this);
